void main(List<String> args) {
  var flag = 1;
  while (flag < 10) {
    print('Iterasi / Looping ke : $flag');
    // atau menggunakan 
    // print('Iterasi / Looping ke : '+ flag.toString());
    flag++;
  }
}