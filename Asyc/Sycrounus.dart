// ini materi di sanbercode
// String createOrderMessage() {
//   var order = fetchUserOrder();
//   return 'Your order is: $order';
// }

// Future<String> fetchUserOrder() => 
//       Future.delayed(
//           Duration(seconds: 2), 
//           () => 'Large Latte'
//       );
// // Imagine that this function is
// // more complex and slow.
// void main() {
//   print('Fetching user order...');
//   print(createOrderMessage());
// }

// ini AI
void main() {
  print('Fetching user order...');
  fetchUserOrder().then((order) {
    print(createOrderMessage(order));
  });
}

String createOrderMessage(String order) {
  return 'Your order is: $order';
}

Future<String> fetchUserOrder() {
  return Future.delayed(Duration(seconds: 2), () => 'Large Latte');
}
