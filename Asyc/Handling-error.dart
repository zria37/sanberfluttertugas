Future<String> createOrderMessage() async {
  try {
    var order = await fetchUserOrder();
    print('Awaiting user order...');
    return 'Your order is: $order';
  } catch (err) {
    print('Caught error: $err');
    return 'Error occurred while fetching order.';
  }
}

Future<String> fetchUserOrder() =>
// Imagine that this function is
// more complex and slow.

    Future.delayed(
      Duration(seconds: 2),
      () => 'Large Latte',
    );
Future<void> main() async {
  print('Fetching user order...');
  print(await createOrderMessage());
}
