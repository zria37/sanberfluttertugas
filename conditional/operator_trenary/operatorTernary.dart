// contoh disini kita akan mengecek apakah seseorang ini benar
// adalah wahyu, jika benar maka print("wahyu") jika tidak maka print("bukan").
// Pertama kita gunakan if-else seperti dibawah ini.

void main() {
  var isThisWahyu = true;
  if (isThisWahyu) {
    print("wahyu");
  } else {
    print("bukan");
  }
}
