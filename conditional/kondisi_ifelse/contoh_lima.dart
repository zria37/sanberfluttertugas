// Contoh 5 Branching dengan kondisi
// Contoh di atas kita memberikan kondisi tambahan yaitu jika minimarket akan
// buka kurang atau sama dengan 5 menit lagi maka saya akan menunggu.
void main() {
  var minimarketStatus = "close";
  var minuteRemainingToOpen = 5;
  if (minimarketStatus == "open") {
    print("saya akan membeli telur dan buah");
  } else if (minuteRemainingToOpen <= 5) {
    print("minimarket buka sebentar lagi, saya tungguin");
  } else {
    print("minimarket tutup, saya pulang lagi");
  }
}
