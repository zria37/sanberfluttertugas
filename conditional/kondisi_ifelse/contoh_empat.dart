// Contoh 4 Branching sederhana
// Kondisi else di atas adalah kondisi selain minimarketStatus == "open"
void main() {
  var minimarketStatus = "open";
  if (minimarketStatus == "open") {
    print("saya akan membeli telur dan buah");
  } else {
    print("minimarketnya tutup");
  }
}
