// contoh kode dalam bahasa dart yang
// menggunakan statement if-else untuk memeriksa apakah sebuah angka bernilai
// lebih besar atau lebih kecil dari 10:
void main() {
  int angka = 5;
  if (angka > 10) {
    print("angka lebih besar dari 10");
  } else {
    print("angka kurang 5");
  }
}
