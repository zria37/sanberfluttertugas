void main(List<String> args) {
  print("LOOPING PERTAMA");
  var i = 2;
  while (i < 20) {
    print("$i - I Love Coding");
    i += 2;
  }

  print("LOOPING KEDUA");
  while (i >= 2) {
    print("$i - I Will Become a Mobile Developer");
    i -= 2;
  }
}