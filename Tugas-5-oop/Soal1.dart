void main(List<String> args) {
  Segitiga segitiga;
  double luasSegitiga;
  segitiga = new Segitiga();
  segitiga.setengah = 0.5;
  segitiga.alas = 20.0;
  segitiga.tinggi = 20;
  luasSegitiga = segitiga.hitungLuas();
  print(luasSegitiga);
}
class Segitiga {
    late double setengah;
    late double alas;
    late double tinggi;
    // catatan late  adalah tidak akan bernilai null
    double hitungLuas(){
       var luas = setengah * alas * tinggi;
        return luas;
    }
}