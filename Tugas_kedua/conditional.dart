import 'dart:io';
/* 1. Ternary operator
Petunjuk : untuk membuat sebuah kondisi ternary dimna anda akan di minta untuk
menginstall aplikasi dengan jawaban Y/T , jadi tugas teman teman sekrang adalah memberi
jawaban y/t saat ada input mau diinstall aplikasi, apabila ( y )maka akan menampilkan "anda
akan menginstall aplikasi dart", jika (T) maka akan keluar pesan "aborted" (Gunakan I/O). */
void main(List<String> args) {
  /* print("Mau di Install Aplikasi ???");
  String? input = stdin.readLineSync();
  if(input == 'Y' || input == 'y'){
    print("Anda akan menginstall aplikasi dart");
  }else{
    print('aborted');
  }

  // atau bisa seperti ini
  print("apakah anda ingin mencetak ?");
  String? valuePrint = stdin.readLineSync();
  valuePrint == "ya" ? print("akan saya print") : print("Aborted"); */

  // Soal No 2
  /* print("nama : ");
  String? masukanNama = stdin.readLineSync();
  print("masukan Peran : ");
  String? masukanPeran = stdin.readLineSync();
  if (masukanNama == "" && masukanPeran == "") {
    print("masukan nama dan juga peran anda");
  } else if (masukanNama != '' && masukanPeran == "") {
    print("Hello $masukanNama Pilih peranmu");
  } else if (masukanNama != '' && masukanPeran == "penyihir") {
    print("selamat datang di Dunia Werewolf, $masukanNama"
            "Halo Penyihir $masukanNama, kamu dapat melihat siapa yang menjadi werewolf!");
  } else if (masukanNama != '' && masukanPeran == "guard") {
    print("Selamat datang di Dunia Werewolf, $masukanNama"
          "Halo Guard $masukanNama, kamu akan membantu melindungi temanmu dari serangan werewolf.");
  } else if (masukanNama != '' && masukanPeran == "werewolf") {
    print("Selamat datang di Dunia Werewolf, $masukanNama"
          ", Halo $masukanNama, kamu akan membantu melindungi temanmu dari serangan werewolf.");
  } else {
    print("kamu akan menjadi penonton");
  } */

  // soal no 3
  /* print("Quote Harian");
  stdout.write("input hari : ");
  String? hari = stdin.readLineSync();
  String quoute;
  switch (hari){
    case "senin":
      {
        quoute = "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.";
        break;
      }
    case "selasa":
      {
        quoute = "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.";
        break;
      }
    case "rabu":
      {
        quoute = "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.";
        break;
      }
    default:
      {
        quoute = "hari yang cerah untuk jiwa yang sepi";
      }
  }
    print(quoute); */

  // Soal No 4
  const tanggal = 12;
  const bulan = 3;
  const tahun = 1945;
  var teksBulan;
  switch (true) {
    case (tanggal < 1 || tanggal > 31):
      {
        print("input tanggal salah");
        break;
      }
    case (tahun < 1900 || tahun > 2200):
      {
        print("input tahun salah");
        break;
      }
    case (bulan > 12 || bulan < 1):
      {
        print("input bulan salah");
        break;
      }
    default:
      {
        switch (true) {
        case bulan == 1:
          teksBulan = "januari";
        break;
        case bulan == 2:
          teksBulan = "februari";
        break;
        case bulan == 3:
          teksBulan = "maret";
        break;
        default:
        break;
      }
    print(tanggal);
    print(teksBulan);
    print(tahun);
    break;
}
}


}