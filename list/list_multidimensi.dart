void main(List<String> args) {
  var arrayMulti = [
                [1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]
  ];
  print(arrayMulti[0][0]); // 1
  print(arrayMulti[1][0]); // 4
  print(arrayMulti[2][1]); // 8
// Maka sebagai gambaran, indeks dari array tersebut adalah
/*
  [ kolom, baris
    [(0,0), (0,1), (0,2)],
    [(1,0), (1,1), (1,2)],
    [(2,0), (2,1), (2,2)]
  ]
*/


}