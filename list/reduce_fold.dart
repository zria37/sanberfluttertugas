void main(List<String> args) {
  // kompres list pada element jadi single value.
  var randomdata = [1, 3, 5, 20, 4, 2];
  randomdata.sort((a, b) => a.compareTo(b));
  print(randomdata);

  var sumData = randomdata.reduce((cur, next) => cur + next);
  print(sumData);
  const currentValue = 10;
  //var currentValue = [1, 2, 3, 4, 5];

  var nextSum = randomdata.fold<int>(currentValue, (cur, next) => cur + next);
  print(nextSum); // 45
}