void main(List<String> args) {
  // function untuk menampilkan tiap-tiap elements
  var perusahaan = ['bukalapak', 'tokopedia', 'blibli'];
  perusahaan.forEach((data)=> print(data)); 
// bukalapak, tokopedia, blibli

// ini untuk menampilkan hanya satu
perusahaan.forEach((data) {
  if (data == 'bukalapak') {
    print(data);
  }
});
}