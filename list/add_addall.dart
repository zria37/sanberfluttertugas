// ini merupakan method yang di pakai untuk menambahkan index pada list kita
// contohnya :
void main(){
  List<int> myList = [];
  List<int> list = [1,2,3];
  myList.add(1);
  myList.addAll(list);
  myList.forEach((bilangan)=>{ print(bilangan) });
}
