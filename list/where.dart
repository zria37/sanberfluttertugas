void main(List<String> args) {
//   where(), firstWhere(), singleWhere()
// mengembalikan nilai list setelah memenuhi kondisi.
List<Map<String, dynamic>> listUser = [
      {'nama': 'bekasi', 'umur': 240},
      {'nama': 'boyolali', 'umur': 200},
      {'nama': 'jakarta', 'umur': 100},
      {'nama': 'surabaya', 'umur': 100},
    ];
  var userYoung = listUser.where((data)=> data['umur'] > 100);
  print(userYoung);

  var userFirstYoung = listUser.firstWhere((data)=> data['umur']< 200);
  print(userFirstYoung); /// {'nama': 'jakarta', 'umur': 100},

  // var userSingle = listUser.singleWhere((data)=> data['umur'] <=100);
  // print(userSingle); /// error karena ada dua kondisi yang benar

/* firstWhere itu ngambil list pertama dari banyak element true, dan singleWhere itu
adalah kondisi true hanya boleh satu.
take(), skip()
ingin mengambil beberapa element dari banyaknya data pada list, gunakan method
take() dan skip() */
  var dataTestCase = [1, 2, 3, 4, 10, 90];
  print(dataTestCase.take(2)); /// (1, 2)
  print(dataTestCase.skip(2)); /// (3, 4, 10, 90)

// expand()
// ingin melakukan flatMap? sangat bisa dengan menggunakan expand()
  var pairs = [[1, 2], ['a', 'b'], [3, 4]];
  var flatmaps = pairs.expand((pair)=> pair);
  print(flatmaps);

/* List Comprehensions
ini bukan method tapi ini adalah update terbaru dari dart 2.3.2 yang mana Dart bisa
membuat list comprehension seperti pada bahasa pemogramman python. */
var comph = [1,2,3,4];
var newCom = [for(var a in comph) 'new ${a}'];
}